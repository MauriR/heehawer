# heehawer

Twitter clone parody based in a tutorial found in youtube. 
https://www.youtube.com/watch?v=ZqgiuPt5QZo&t=8294s&ab_channel=TheEarthisSquare

The tutorial has, obvioulsy my personal touch and I developed since the beginning further options and features. 
A great oportunity to keep learning VUE, my favourite Framework.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
