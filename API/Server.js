const express = require('express')
const port = process.env.port || 8081
const cors = require('cors')
const bodyParser = require('body-parser')

const app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors({ origin: /http:\/\/localhost/ }))
app.options('*', cors())

app.listen(port, () => {
  console.log(`Broadcasting live from ${port}`)
}) 