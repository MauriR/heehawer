export const quotes = [
  {
    id:1,
    'quote': 'No donkeys were damaged during the creation of this Network'
  },

  {
    id:2,
    'quote': 'The 1 out of 10 dentists who does not recommend people to brush their teeth in the commercials, recommends you Heehawer over Twitter'
  },

  {
    id:3,
    'quote': 'Donkey Fact #1: A donkey\'s bray can carry up to 95 km. in the desert.'
  },

  {
    id:4,
    'quote': 'Donkey Fact #2: Donkeys can see all four of their feet at the same time.'
  },

  {
    id:5,
    'quote': 'Donkey Fact #3: Donkeys have incredibly efficient digestive systems, utilising 95% of what they eat.'
  },

  {
    id:6,
    'quote':' Donkey Fact #4: Donkeys don\'t like being in the rain for long periods as their fur is not waterproof'
  },

  {
    id:7,
    'quote': 'Donkey Fact #5: Donkeys are called Burros in Spanish, Esel in German and Âne in French'
  },

  {
    id:8,
    'quote': 'Donkey Fact #6: In english, male donkeys are called jacks and female donkeys are called jennies or jennets.'
  },

  {
    id:9,
    'quote': 'Donkey Fact #7: Donkeys love bananas, and they aren\'t put off by the bitter taste of the peel like humans are.'
  },

  {
    id:10,
    'quote':'Donkey Fact #8: Donkeys can be trained to respond to dozens of different voice commands,'
  },

  {
    id:11,
    'quote': 'Donkey Fact #9: Donkeys have amazing memories that can recognize a person or route that they haven\'t seen for years'
  },

  {
    id:12,
    'quote': 'Donkey Fact #10: Donkeys have big ears because they use them to cool down when they get overheated. This is important since so many of them live in desert climates.'
  },

  {
    id:13,
    'quote': 'Donkey Fact #11: Female donkeys have one of the longest gestation periods in the entire animal kingdom. They\'re pregnant for anywhere between 10 – 14 months!'
  },

  {
    id:14,
    'quote': 'Donkey Fact #12: Speaking about Hee-haw, donkeys are one of the only species that can make noise while they\'re breathing in and out.'
  },

  {
    id:15,
    'quote': 'Donkey Fact #13: Donkey milk is completely hypoallergenic'
  },

  {
    id:16,
    'quote': 'Donke Fact # 14: Donkeys have exhibited signs of grief after the death of a herd member.'
  },

  {
    id:17,
    'quote': 'Donkeys proverbs and expressions #1: Donkey\'s years. If you haven\'t seen someone in a donkey\'s years, you haven\'t seen them in a long time.'
  },

  {
    id:18,
    'quote': 'Donkeys proverbs and expressions #2: Pin the tail on the donkey. This is a children\'s game involving blindfolds that can also be used to describe a pointless or frustrating task.'
  },

  {
    id:19,
    'quote': 'Donkeys proverbs and expressions #3: Talk the hind legs off a donkey. Since donkeys don\'t usually sit or lie down, this expression means that someone has talked for so long that they\'ve even managed to wear down a donkey.'
  },

  {
    id:20,
    'quote': 'Donkeys proverbs and expressions #3: A donkey in a lion\'s skin. This comes from an old fable about a donkey dressing up in a lion\'s skin to fool the other animals. Eventually, he brayed and gave himself away, so the moral of the story is that people can\'t fool you once they open their mouths.'
  },
  


  
  
]