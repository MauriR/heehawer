export const users = [

  {
    "userId": 1,
    "userName": "Supreme_Donkey",
    "firstName": "Usuario",
    "lastName": "De Muestra",
    "email": "manuManolez@fakemail.com",
    "isAdmin": true,
    "password": 'supreme',
    "heehaws": [
      {
        "id": 0,
        "content": "Rebuzno de muestra",
        "fav": 0
      }
    ]
  },

  {
    "userId": 2,
    "userName": "UL_a_abono",
    "firstName": "Esther",
    "lastName": "Colero",
    "email": "ecolero@fakemail.com",
    "isAdmin": false,
    "password":"colero",
    "heehaws": [{
      "id": 0,
      "content": "Rebuzno de muestra",
      "fav": 0
    }]
  },

  {
    "userId": 3,
    "userName": "J_olaj_",
    "firstName": "Juaquín",
    "lastName": "Olajubón",
    "email": "Joola@fakemail.com",
    "isAdmin": false,
    "password":"olajubon",
    "heehaws": [
      {
        "id": 0,
        "content": "Rebuzno de muestra",
        "fav": 0
      }
    ]
  },

  {
    "userId": 4,
    "userName": "Mark91",
    "firstName": "Mark",
    "lastName": "smith",
    "email": "Masmi@fakemail.com",
    "isAdmin": false,
    "password": "esmiz",
    "heehaws": [
      {
        "id": 0,
        "content": "Rebuzno de muestra",
        "fav": 0
      }
    ]
  }

]
