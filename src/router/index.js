import { createRouter, createWebHistory } from 'vue-router';
import store from '../store'
import { users } from '../assets/users'
import Home from '../views/Home'
import Register from '../views/Register'
import UserProfile from '../views/HUserProfile'
import Admin from '../views/Admin'

const routes = [
  // {
  //   path:'/lazy',
  //   name:'Lazy',
  //   component:()=>import ('../views/Lazy.vue')
  // } Ejemplo de lazy loading de una ruta. 
  {
    path: '/',
    name: 'Home',
    component: Home
  },

  {
    path: '/register',
    name: 'Register',
    component: Register
  },

  {
    path: '/user/:userId', //path dinámico
    name: 'UserProfile',
    component: UserProfile
  },

  {
    path: '/admin',
    name: 'Admin',
    component: Admin,
    meta: {
      requiresAdmin: true
    }
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})
router.beforeEach(async (to, from, next) => {//siempre 3 variables
  const user = store.state.User.user

  if (!user) {// en cuanto haya base de datos se cambia, ahora es fake
    await store.dispatch('User/setUser',{isAdmin:false}) // dispatch es la función que llamamos para runnear acciones. Parecido al store.commit de las mutaciones

  }

  const isAdmin = store.state.User.user.isAdmin
  const requiresAdmin = to.matched.some(record => record.meta.requiresAdmin)

  if (requiresAdmin && isAdmin == false) next({ name: 'Home' })
  else next()
  
}) //se supone que es para poder pedir que el usuario sea admin. Ahora mismo está en true y deja entrar a todos al /admin. Si no, no dejaría.
//update ahora tira de store. Está fakelogged con el user 0 y por lo tanto deja porque es el admin. Si cambiamos al user 1 pues ya no deja. 

export default router
