export const UserModule = {
namespaced:true,
state: {
  user:null,
}, 


mutations: { //funciones que afectan al estado (state). EN Mayus como convención.
SET_USER(state, user){
state.user=user
}
},


actions: { //funciones a las que se llama desde la aplicación y que llaman a mutations
setUser({commit}, user){
commit('SET_USER' ,  user)
}
},
}