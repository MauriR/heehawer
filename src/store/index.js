import Vuex from 'vuex'
import {UserModule} from './User'

export default Vuex.createStore({
  state: {
  }, 


  mutations: { //funciones que afectan al estado (state). EN Mayus como convención.
  },

  
  actions: { //funciones a las que se llama desde la aplicación y que llaman a mutations
  },


  modules: {
   User:UserModule,
  }
});
